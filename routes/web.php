<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.table');
});

Route::get('/data-tables', function () {
    return view('items.data-tables');
});

/* Route::get('/pertanyaan', 'PertanyaanController@index')->name('pertanyaan.index');
Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::POST('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
Route::PUT('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
Route::DELETE('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@destroy'); */

Route::resource('pertanyaan','PertanyaanController');