@extends('master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> <b>{{$pertanyaan->judul}}</b></h4>
        <p> {{$pertanyaan->isi}}</p>
    </div>
@endsection